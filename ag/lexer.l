%{
	#include "y.tab.h"
	void yyerror(char *);
	char *copy_lexeme();
%}

keyword		(end|array|of|int|return|if|then|else|while|do|var|not|or)
specialchar	(;|\(|\)|\,|\:|\<|\#|\[|\]|\-|\+|\*)
assign		\:\=
identifier	[a-zA-Z]+[a-zA-Z0-9]*
decnumber	[0-9]+
hexnumber	\$[0-9a-fA-F]+
%%
[ \t\n]+		
\-\-.*						
end		return END;
array	        return ARRAY;
of		return OF;
int		return INT;
return		return RETURN;
if		return IF;
then		return THEN;
else		return ELSE;
while		return WHILE;
do		return DO;
var		return VAR;
not		return NOT;
or		return OR;
{assign}	return ASSIGN;
{specialchar}	return yytext[0];
{identifier}	return ID; @{ @ID.x@ = copy_lexeme (); @}
{decnumber}	return NUM;
{hexnumber}	return NUM;
.		{ printf("bad input character '%s' at line %d\n", yytext, yylineno); exit(1); }

%%

char *copy_lexeme()
{
    char *cpy = NULL;
    cpy = malloc (yyleng + 1);

    if (cpy == NULL)
    {
	printf ("Unable to allocate a buffer for the lexeme %s", yytext);
	exit (100);
    }

    printf ("Copying lexeme %s, yyleng: %i\n", yytext, yyleng);
    strncpy (cpy, yytext, yyleng+1);
    return cpy;
}

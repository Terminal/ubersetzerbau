%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    enum type { INTEGER }; 
 
    struct symbol_type {
    	enum type symb_type;   

	/* the nesting depth of arrays, 0 if no array */
	int depth;    
    };
    
    struct symtable_entry {
 	/* the name of the identifier */
	char *name;

	struct symbol_type *type;

	/* list meta data */
	struct symtable_entry *next;
	struct symtable_entry *prev;
    };
    
    struct symtable_entry *append_to_symtable (struct symtable_entry *last, struct symtable_entry *lastother);
    struct symtable_entry *create_symtable_entry (char *name, struct symbol_type *type); 
    struct symtable_entry *lookup_from_symboltable(char *name, struct symtable_entry *last);
    struct symbol_type *create_int_symbol_type (int depth);

    void check_assignment (struct symbol_type *first_type, struct symbol_type *second_type);
    void check_arithmetic_expression (struct symbol_type *first_type, struct symbol_type *second_type);
    void check_symt_collision (struct symtable_entry *last);
    void check_is_array_type (struct symbol_type *type);
    void check_is_int_type (struct symbol_type *type);
    struct symbol_type *get_type_of_id (char *name, struct symtable_entry *last);
    void print_symbol_table (struct symtable_entry *last);

%}

%start Program
%token ID NUM END ARRAY OF INT RETURN IF THEN ELSE WHILE DO VAR NOT OR ASSIGN

@autoinh symt

/* inherited attributes */
@attributes { struct symtable_entry *symt; } Stats Bool Bterm Boolrest Callparam Callparamrest

/* synthesized attributes */
@attributes { struct symtable_entry *symtup; } Pars Vardefs Vardef Vardefrest
@attributes { char *x; } ID
@attributes { int depth; } Type Arrayinit

/* both */
@attributes { struct symtable_entry *symt; struct symtable_entry *symtup; } Stat
@attributes { struct symtable_entry *symt; struct symbol_type *typeup; } Expr Lexpr Term Subterm Addterm Multterm

@traversal @postorder typecheck
%%

Program:	/* empty */
		| Funcdef ';' Program
		;

Funcdef:	ID '(' Pars ')' Stats END
		@{ @i @Stats.0.symt@ = @Pars.0.symtup@; @}			
		;

Pars:		/* empty */
		@{ @i @Pars.0.symtup@ = NULL; @}

		| Vardefs
		@{ 
		   @i @Pars.0.symtup@ = @Vardefs.0.symtup@; 
		   @typecheck check_symt_collision (@Vardefs.0.symtup@);
		@}
		;

Vardefs:	Vardef Vardefrest
		@{ @i @Vardefs.0.symtup@ = append_to_symtable (@Vardef.0.symtup@, @Vardefrest.0.symtup@); @}
		;

Vardef:		ID ':' Type
		@{ @i @Vardef.0.symtup@ = create_symtable_entry (@ID.0.x@, create_int_symbol_type(@Type.0.depth@)); 
		@}
		;

Vardefrest:	/* empty */
		@{ @i @Vardefrest.0.symtup@ = NULL; @}
		
		| Vardefrest ',' Vardef
		@{ @i @Vardefrest.0.symtup@ = append_to_symtable (@Vardefrest.1.symtup@, @Vardef.0.symtup@); @}
		;

Type:		Arrayinit INT
		@{ @i @Type.0.depth@ = @Arrayinit.0.depth@; @}
		;

Arrayinit:	/* empty */
		@{ @i @Arrayinit.0.depth@ = 0; @}
		| ARRAY OF Arrayinit
		@{ @i @Arrayinit.0.depth@ = 1 + @Arrayinit.1.depth@; @}
		;

Stats:		/* empty */
		| Stat ';' Stats
		@{ 
		   /* The symbol table may have changed in the first part because of a variable definition */
		    @i @Stats.1.symt@ = @Stat.0.symtup@; 
		@}

Stat:		RETURN Expr
		@{ @i @Stat.0.symtup@ = @Stat.0.symt@; @}

		| IF Bool THEN Stats END
		@{ @i @Stat.0.symtup@ = @Stat.0.symt@; @}
		
		| IF Bool THEN Stats ELSE Stats END
		@{ @i @Stat.0.symtup@ = @Stat.0.symt@; @}

		| WHILE Bool DO Stats END
		@{ @i @Stat.0.symtup@ = @Stat.0.symt@; @}
		
		| VAR Vardef ASSIGN Expr
		@{ 
		    @i @Stat.0.symtup@ = append_to_symtable (@Stat.0.symt@, @Vardef.0.symtup@);
		    @typecheck	check_assignment (@Vardef.0.symtup@->type, @Expr.0.typeup@);			
		@}

		| Lexpr ASSIGN Expr
		@{ 
		    @i @Stat.0.symtup@ = @Stat.0.symt@;
		    @typecheck check_assignment (@Lexpr.0.typeup@, @Expr.0.typeup@);
		@}   
		   
		| Term
		@{ 
		   @i @Stat.0.symtup@ = @Stat.0.symt@;
		@}
		;

Bool:		Bterm Boolrest
		;

Boolrest:	/* empty */
		| OR Bool
		;

Bterm: 		'(' Bool ')'
       		| NOT Bterm
		| Expr '<' Expr
		@{ @typecheck check_arithmetic_expression (@Expr.0.typeup@, @Expr.1.typeup@); @}

		| Expr '#' Expr
		@{ @typecheck check_arithmetic_expression (@Expr.0.typeup@, @Expr.1.typeup@); @}
		;

Lexpr:		ID
		@{ @i @Lexpr.0.typeup@ = get_type_of_id (@ID.0.x@, @Lexpr.0.symt@); @}
		| Term '[' Expr ']'
		@{ 
		   @i @Lexpr.0.typeup@ = create_int_symbol_type (@Term.0.typeup@->depth - 1); 
		   @typecheck check_is_array_type (@Term.0.typeup@); check_is_int_type (@Expr.0.typeup@);
		@}
		; 

Expr:		Term
		@{ @i @Expr.0.typeup@ = @Term.0.typeup@; @}
		
		/* because the types of all NTs have to be the same, we can just the first one */
		| Term '-' Term Subterm
		@{ 
		    @i @Expr.0.typeup@ = @Term.0.typeup@; 
 		    @typecheck 
			check_arithmetic_expression (@Term.0.typeup@, @Term.1.typeup@);
		    	check_arithmetic_expression (@Term.0.typeup@, @Subterm.0.typeup@);
		@}

		| Term '+' Term Addterm
		@{ 
		    @i @Expr.0.typeup@ = @Term.0.typeup@; 
		    @typecheck 
			check_arithmetic_expression (@Term.0.typeup@, @Term.1.typeup@);
		    	check_arithmetic_expression (@Term.0.typeup@, @Addterm.0.typeup@);
		@}

		| Term '*' Term Multterm
		@{ 
		    @i @Expr.0.typeup@ = @Term.0.typeup@; 
		    @typecheck 
			check_arithmetic_expression (@Term.0.typeup@, @Term.1.typeup@);
		    	check_arithmetic_expression (@Term.0.typeup@, @Multterm.0.typeup@);
		@}
		;

Subterm:	/* empty */
		@{ @i @Subterm.0.typeup@ = NULL; @}

		| '-' Term Subterm
		@{ 
		    @i @Subterm.0.typeup@ = @Term.0.typeup@; 
		    @typecheck check_arithmetic_expression (@Term.0.typeup@, @Subterm.0.typeup@);
		@}
		;

Addterm:	/* empty */
		@{ @i @Addterm.0.typeup@ = NULL; @}

		| '+' Term Addterm
		@{ 
		    @i @Addterm.0.typeup@ = @Term.0.typeup@; 
		    @typecheck check_arithmetic_expression (@Term.0.typeup@, @Addterm.0.typeup@);
		@}
		;

Multterm:	/* empty */
		@{ @i @Multterm.0.typeup@ = NULL; @}

		| '*' Term Multterm
		@{ 
		    @i @Multterm.0.typeup@ = @Term.0.typeup@;
		    @typecheck check_arithmetic_expression (@Term.0.typeup@, @Multterm.0.typeup@);
		@}
		;

Term:		'(' Expr ')'
		@{ @i @Term.0.typeup@ = @Expr.0.typeup@; @}

		| NUM
		@{ @i @Term.0.typeup@ = create_int_symbol_type (0); @}

		| Term '[' Expr ']'
		/* this term reduces the nesting depth of the array type */
		@{ 
		    @i @Term.0.typeup@ = create_int_symbol_type (@Term.1.typeup@->depth - 1); 
		    @typecheck check_is_array_type (@Term.1.typeup@); check_is_int_type (@Expr.0.typeup@);
		@}

		| ID
		@{  
		    @i @Term.0.typeup@ = get_type_of_id (@ID.0.x@, @Term.0.symt@);	
		@}

		| ID '(' Callparam ')' ':' Type
		/* because functions are not saved in the symbol table we can create a new type here */
		@{ @i @Term.0.typeup@ = create_int_symbol_type (@Type.0.depth@); @}
		;

Callparam:	/* empty */
		| Expr
		| Expr Callparamrest
		;

Callparamrest:	',' Expr Callparamrest
		| ',' Expr
		;

%%

void print_symbol_table (struct symtable_entry *last)
{
    struct symtable_entry *current = NULL;

    printf ("Symbol table in reverse order: \n");
    
    current = last;
    while (current != NULL)
    {
	printf ("%s, (%i, %i) <- ", current->name, current->type->symb_type, current->type->depth);
	current = current->prev;
    }
    printf ("\n");
}

struct symbol_type *get_type_of_id (char *name, struct symtable_entry *last)
{
    struct symtable_entry *current = NULL;
    current = last;

    while (current != NULL)
    {
	if (strcmp (current->name, name) == 0)
	{
	    return current->type;
	}

	current = current->prev;
    }

    printf ("Undeclared identifier found: %s\n", name);
    print_symbol_table (last);
    exit (3);
}

void check_assignment (struct symbol_type *first_type, struct symbol_type *second_type)
{
    if (first_type == NULL || second_type == NULL)
    {
	printf ("check_assignment: One of the supplied types is NULL\n");
	exit (100);
    }

    if (!is_symbol_type_equal (first_type, second_type))
    {
	printf ("Invalid assignment detected: ");
	printf ("Type depth %i assigned to type depth %i\n", first_type->depth, second_type->depth);
	exit (3);
    }
}

void check_arithmetic_expression (struct symbol_type *first_type, struct symbol_type *second_type)
{
    if (first_type == NULL)
    {
	printf ("check_arithmetic_expression: The required type is NULL\n");
	exit (100);
    }

    if (first_type->depth != 0 || first_type->symb_type != INTEGER)
    {
	printf ("Only basic integer types are allowed in an arithmetic expression\n");
	exit (3);
    }

    if (second_type != NULL)
    {
	if (!is_symbol_type_equal (first_type, second_type))
	{
	    printf ("Invalid expression detected: ");
	    printf ("Both types of an expression must be equal\n");
	    exit (3);
	}
    }
}

void check_symt_collision (struct symtable_entry *last)
{
    struct symtable_entry *current = NULL;
    struct symtable_entry *currentother = NULL;

    if (last == NULL)
    {
	/* no collision possible */
	return;
    }

    current = last;
    currentother = last;
    while (current != NULL)
    {
	while (currentother != NULL)
	{

	    
	    if (current != currentother && strcmp (current->name, currentother->name) == 0)
	    {
		printf ("Duplicate identifier %s detected\n", current->name);
		print_symbol_table (last);
		exit(3);
	    }
	    currentother = currentother->prev;
	}

	currentother = last;
	current = current->prev;
    }
}

void check_is_array_type (struct symbol_type *type)
{
    if (type == NULL)
    {
	printf ("check_is_array_type: supplied type is NULL");
	exit (100);
    }

    if (type->depth == 0 )
    {
	printf ("Invalid type detected. Should be an array type but depth is 0\n");
	exit (3);
    }
}

void check_is_int_type (struct symbol_type *type)
{
    if (type == NULL)
    {
	printf ("check_is_int_type: supplied type is NULL");
	exit (100);
    }

    if (type->symb_type != INTEGER || type->depth != 0)
    {
	printf ("Invalid type detected. Should be %i (0) but is %i (%i)\n", INTEGER, type->symb_type, type->depth);
	exit (3);
    }
}

struct symtable_entry *append_to_symtable (struct symtable_entry *last, struct symtable_entry *lastother) {
    struct symtable_entry *current = NULL;

    if (lastother == NULL)
       return last;

    if (last == NULL)
	return lastother;	

    current = lastother;

    while (current->prev != NULL)
	current = current->prev;

    last->next = current;
    current->prev = last;

    return lastother;
}

struct symtable_entry *lookup_from_symboltable (char *name, struct symtable_entry *last)
{
    struct symtable_entry *current = NULL;
    current = last;

    while (current != NULL)
    {
        if (strcmp (last->name, name) == 0)
        {
            return last;
        }

	current = current->prev;
    }

    return NULL;
}

struct symtable_entry* create_symtable_entry (char *name, struct symbol_type *type)
{
    struct symtable_entry *newentry = NULL;
    newentry = malloc (sizeof (struct symtable_entry));

    if (newentry != NULL)
    {
	newentry->name = name;
	newentry->type = type;
	return newentry;
    }

    printf("Unable to allocate a new symtable entry\n");
    exit(100);

    return NULL;
}

struct symbol_type *create_int_symbol_type (int depth)
{
    struct symbol_type *newsymbtype = NULL;
    newsymbtype = malloc (sizeof (struct symbol_type));

    if(newsymbtype != NULL)
    {
	newsymbtype->symb_type = INTEGER;
	newsymbtype->depth = depth;

	return newsymbtype;
    }

    printf("Unable to allocate a new symbol type\n");
    exit(100);

    return NULL;
}

int is_symbol_type_equal (struct symbol_type *first, struct symbol_type *second)
{
    if(first == NULL || second == NULL)
    {
	return 0;
    }

    return first->symb_type == second->symb_type 
	&& first->depth == second->depth;
}

int yywrap(void) {
    return 1;
}

int yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
    exit(2);
}

int main(void) {
    yyparse();
    return 0;
}

#include "codegen_asm.h"

static int used_param_registers[6];
static char *param_registers[] = { "rdi", "rsi", "rdx", "rcx", "r8", "r9" };
static int used_registers[3];
static char *registers[] = { "rax", "r10", "r11" };

static int lblcount = 0;

char *new_label (void)
{
    char *buf = NULL;
    int size;
    lblcount++;

    size = (lblcount/10)+3;
    buf = malloc (size);

    if (buf == NULL)
    {
        printf ("Unable to allocate a label buffer\n");
        exit (100);
    }

    snprintf (buf, size, "L%i", lblcount);
    return buf;
}

void assign_parameter_regs (struct symtable_entry *last)
{
    struct symtable_entry *cur = last;
   
    if (last == NULL)
	return;

    while (cur->prev != NULL)
    {
	cur = cur->prev;
    }

    /* Iterate over all parameters */
    while (cur != last->next)
    {
	cur->regname = get_next_parameter_reg ();
	cur = cur->next;
    }

}

void assign_parameter_stackpositions (struct parameter *param, struct symtable_entry *last)
{
    int first = get_next_stackposition (last);
    
    struct parameter *cur = param;

    if (param == NULL)
	return;
 
    while (cur->previous != NULL)
    {
	cur = cur->previous;
    }
   
    while (cur != param->next)
    {
	cur->stackposition = first++;
	cur = cur->next;
    }
}

static char *find_reg (int *usage_bitmap, char **register_map, int num_registers)
{
    int i=0;

    for (i=0; i < num_registers; i++)
    {
	if (usage_bitmap[i] == 0)
	{
	    usage_bitmap[i] = 1;
	    return register_map[i];
	}
    }

    return NULL;
}


char *get_next_parameter_reg (void)
{
    int num_registers = sizeof (used_param_registers) / sizeof (int);
    char *result_reg = NULL;

    result_reg = find_reg (used_param_registers, param_registers, num_registers);

    if (result_reg != NULL)
    {
	return result_reg;
    }

    printf ("No parameter registers left\n");
    exit (100);
}

char *get_next_reg (void)
{
    int num_registers = sizeof (used_registers) / sizeof (int);
    char *result_reg = NULL;

    result_reg = find_reg (used_registers, registers, num_registers);

    if (result_reg != NULL)
    {
	#ifdef TREE
	printf ("Assigning normal register %s\n", result_reg);
	#endif
	return result_reg;
    }

    result_reg = get_next_parameter_reg ();

    if (result_reg != NULL)
    {
	#ifdef TREE
	printf ("Assigning parameter register %s\n", result_reg);
	#endif
	return result_reg;
    }

    printf ("No registers left \n");
    exit (100);
}

void free_regname (char *name)
{
    int num_param_registers = sizeof (used_param_registers) / sizeof (int);
    int num_registers = sizeof (used_registers) / sizeof (int);
    int i;

    #ifdef TREE
    printf ("Freeing register %s\n", name);
    #endif

    for (i=0; i < num_param_registers; i++)
    {
	if (strcmp (name, param_registers[i]) == 0)
	{
	    used_param_registers[i] = 0;
	    return;
	}
    }

    for (i=0; i < num_registers; i++)
    {
	if (strcmp (name, registers[i]) == 0)
	{
	    used_registers[i] = 0;
	    return;
	}
    }    
}

void free_reg (treenodep tree)
{
    char *name;

    assert (tree->kids[0] != NULL);
    assert (tree->kids[1] != NULL);

    if (tree->kids[0]->op == CONST)
    {
	name = tree->kids[0]->regname;
    }
    else
    {
	name = tree->kids[1]->regname;
    }

    free_regname (name);
}

void function_start (char *name, int varcount)
{
    printf (".globl %s\n", name);
    printf ("\t.type\t%s, @function\n", name);
    printf ("%s:\n", name);
    printf ("\tenter $%i, $0\n", varcount * 8);
}

void function_end ()
{
    memset(used_param_registers, 0, sizeof (used_param_registers));
    memset(used_registers, 0, sizeof (used_registers));
}

void return_reg (char *regname)
{
    move (regname, "rax");
    printf ("\tleave\n");
    printf ("\tret\n");
}

void return_const (int value)
{
    move_const (value, "rax");
    printf ("\tleave\n");
    printf ("\tret\n");
}

void move_const (int value, char *regname)
{
    printf ("\tmovq $%i, %%%s\n", value, regname);
}

void move (char *regname1, char *regname2)
{

    if (strcmp (regname1, regname2) == 0)
    {
	return;
    }

    printf ("\tmovq %%%s, %%%s\n", regname1, regname2);
}

void add (char *regname1, char *regname2)
{
    printf ("\taddq %%%s, %%%s\n", regname1, regname2);
}

void add_const (int value, char *regname)
{
    if (value == 0)
    {
	return;
    }

    printf ("\taddq $%i, %%%s\n", value, regname);
}

void mul (char *regname1, char *regname2)
{
    printf ("\timulq %%%s, %%%s\n", regname1, regname2);
}

void mul_const (int value, char *regname)
{
    printf ("\timulq $%i, %%%s\n", value, regname);
}

void sub (char *regname1, char *regname2)
{
    printf ("\tsubq %%%s, %%%s\n", regname1, regname2);
}

void sub_const_left (int value, char *regname)
{
    /* Because there is no command for constant - register
       use the following equivalence:
       10 - x = r | * (-1)
       -10 + x = -r
       r = (-1) * (-r)
    */

    value = value * -1;
    add_const (value, regname);
    printf ("\tneg %%%s\n", regname);
}

void sub_const_right (char *regname, int value)
{
    printf ("\tsubq $%i, %%%s\n", value, regname);
}

void array_const (char *regname, int index, char *dstreg)
{
    printf ("\tmovq %i(%%%s), %%%s\n", (index * 8), regname, dstreg); 
}

void array (char *basereg, char *indexreg, char *dstreg)
{
    printf ("\tmovq (%%%s,%%%s, 8), %%%s\n", basereg, indexreg, dstreg);
}

static void bool_trailer (char *falselbl, char *afterjmplbl)
{
    jmp (falselbl);

    if (afterjmplbl != NULL)
	label (afterjmplbl);
}

void bool_smaller (char *regname1, char *regname2, treenodep boolnode)
{
    printf ("\tcmp %%%s, %%%s\n", regname2, regname1);
    printf ("\tjl %s\n", boolnode->truelbl);
    bool_trailer (boolnode->falselbl, boolnode->afterjmplbl);
}

void bool_smaller_const (int value1, int value2, treenodep boolnode)
{
    if (value1 < value2)
	jmp (boolnode->truelbl);

    bool_trailer (boolnode->falselbl, boolnode->afterjmplbl);
}

void bool_not_equal (char *regname1, char *regname2, treenodep boolnode)
{
    printf ("\tcmp %%%s, %%%s\n", regname1, regname2);
    printf ("\tjne %s\n", boolnode->truelbl);
    bool_trailer (boolnode->falselbl, boolnode->afterjmplbl);
}

void bool_not_equal_const (int value1, int value2, treenodep boolnode)
{
    if (value1 != value2)
	jmp (boolnode->truelbl);
    
    bool_trailer (boolnode->falselbl, boolnode->afterjmplbl);
}

void label (char *lbl)
{
    printf ("\t%s:\n", lbl);
}

void if_then (char *afterjmplbl, char *endlbl)
{
    if (afterjmplbl != NULL)
    {
	jmp (afterjmplbl);
    }

    label (endlbl);
}

void jmp (char *lbl)
{
    printf ("\tjmp %s\n", lbl);
}

void assign_var (char *srcreg, char *dstreg, int stackposition)
{
    /* check if a parameter is assigned */
    if (dstreg != NULL)
    {
	move (srcreg, dstreg);
	return;
    }

    assert (stackposition >= 0);

    int rbpoffset = stackposition * 8;
    printf ("\tmovq %%%s, -%i(%%rbp)\n", srcreg, rbpoffset);
}

void assign_var_const (int value, char *dstreg, int stackposition)
{
    /* check if a parameter is assigned */
    if (dstreg != NULL)
    {
	move_const (value, dstreg);
	return;
    }

    assert (stackposition >= 0);

    int rbpoffset = stackposition * 8;
    printf ("\tmovq $%i, -%i(%%rbp)\n", value, rbpoffset);
}


void assign_array (char *srcreg, char *basereg, char *indexreg)
{
    printf ("\tmovq %%%s, (%%%s,%%%s, 8)\n", srcreg, basereg, indexreg);
}

void var_access (char *regname, int stackposition, char *dstreg)
{
    if (regname != NULL)
    {
	move (regname, dstreg);
	return;
    }

    int rbpoffset = stackposition * 8;
    printf ("\tmovq -%i(%%rbp), %%%s\n", rbpoffset, dstreg);
}

static void save_registers (char **regnames, int *usage_bitmap, int num_registers, char *resultexclusion)
{
    int i;
    for (i=0; i<num_registers; i++)
    {
	if (usage_bitmap[i] == 1 && strcmp (resultexclusion, regnames[i]) != 0)
	{
	    push (regnames[i]);
	}
    }
}

static void restore_registers (char **regnames, int *usage_bitmap, int num_registers, char *resultexclusion)
{
    int i;
    for (i = num_registers-1; i>= 0; i--)
    {
	if (usage_bitmap[i] == 1 && strcmp (resultexclusion, regnames[i]) != 0)
	{
	    pop (regnames[i]);
	}
    }
}

void push (char *reg)
{
    printf ("\tpushq %%%s\n", reg);
}

void pop (char *reg)
{
    printf ("\tpopq %%%s\n", reg);
}

void parameter (callparam *param, char *srcreg)
{
    assert (param != NULL);
    assign_var (srcreg, NULL, param->stackposition);
}

void save_to_stack (char *resultexclusion)
{
    int num_param_registers = sizeof (used_param_registers) / sizeof (int);
    int num_registers = sizeof (used_registers) / sizeof (int);

    save_registers (param_registers, used_param_registers, num_param_registers, resultexclusion);
    save_registers (registers, used_registers, num_registers, resultexclusion);
}

void restore_from_stack (char *resultexclusion)
{
    int num_param_registers = sizeof (used_param_registers) / sizeof (int);
    int num_registers = sizeof (used_registers) / sizeof (int);

    restore_registers (registers, used_registers, num_registers, resultexclusion);
    restore_registers (param_registers, used_param_registers, num_param_registers, resultexclusion);
}

void function_call (char *name, struct parameter *callparam, char *dstreg)
{
    struct parameter *cur = callparam;
    int paramcount = 0;

    while (cur != NULL)
    {
	paramcount++;

	if (cur->position <= 6)
	{
	    var_access (NULL, cur->stackposition, param_registers[cur->position]);
	}						

	cur = cur->previous;
    }

    printf ("\tcall %s\n", name);
    move ("rax", dstreg);
    restore_from_stack (dstreg);
}

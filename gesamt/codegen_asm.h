#ifndef CODEGEN_ASM_H_DEFINED
#define CODEGEN_ASM_H_DEFINED

#include <stdio.h>
#include <stdlib.h>
#include "symtable.h"
#include "codegen.h"
#include <assert.h>
#include <string.h>

char *new_label (void);

void assign_parameter_regs (struct symtable_entry *last);
void assign_parameter_stackpositions (struct parameter *param, struct symtable_entry *last);

void free_reg (treenodep tree);
void free_regname (char *name);

char *get_next_reg (void);
char *get_next_parameter_reg (void);

void function_start (char *name, int varcount);
void function_end ();
char *get_next_const_reg (void);
char *get_next_parameter_reg (void);

void function_end (void);

void return_reg (char *regname);
void return_const (int value);

void move (char *regname1, char *regname2);
void move_const (int value, char *regname);

void sub (char *regname1, char *regname2);
void sub_const_left (int value, char *regname);
void sub_const_right (char *regname, int value);

void add (char *regname1, char *regname2);
void add_const (int value, char *regname);

void mul (char *regname1, char *regname2);
void mul_const (int value, char *regname);

void array (char *regname1, char *reganme2, char *dstreg);
void array_const (char *regname, int index, char *dstreg);

void bool_smaller (char *regname1, char *regname2, treenodep boolnode);
void bool_smaller_const (int value1, int value2, treenodep boolnode);
void bool_not_equal (char *regname1, char *regname2, treenodep boolnode);
void bool_not_equal_const (int value1, int value2, treenodep boolnode);

void label (char *lbl);
void jmp (char *dst);

void assign_var (char *src, char *reg, int stackposition);
void assign_var_const (int value, char *reg, int stackposition);
void assign_array (char *src, char *base, char *index);

void push (char *reg);
void pop (char *reg);

void save_to_stack (char *resultexclusion);
void restore_from_stack (char *resultexclusion);
void parameter (callparam *param, char *srcreg);
void function_call (char *name, struct parameter *callparam, char *dstreg);

#endif

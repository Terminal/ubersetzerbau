#ifndef CODEGEN_TREE_H_INCLUDED
#define CODEGEN_TREE_H_INCLUDED

#include "codegen.h"
#include "codegen_asm.h"
#include "symtable.h"

treenodep create_operator_node (enum operator op, treenodep left, treenodep right);
treenodep combine_arithmetic_trees (treenodep left, treenodep right);
treenodep create_const_node (int const_value);
treenodep create_reg_node (char *name);
treenodep create_var_node (void);
treenodep create_bool_node (enum operator op, treenodep left, treenodep right, char *truelbl, char *falselbl);
treenodep create_stats_node (treenodep left, treenodep right);
treenodep create_if_node (treenodep left, treenodep right, char *falselbl, char *afterjmplbl);
treenodep create_if_else_node (treenodep bool, treenodep statsthen, treenodep statselse, char *falselbl);
treenodep create_label_node (char *lbl);
treenodep create_while_node (treenodep bool, treenodep stats, char *falselbl);
treenodep create_var_assign_node (treenodep left);
treenodep create_array_assign_node (treenodep array, treenodep index);

treenodep create_save_node (void);
treenodep create_param_node (treenodep expr, callparam *params);
treenodep create_function_call_node (char *name, treenodep params, callparam *param);

callparam *create_parameter (callparam *last);
void configure_parameter (callparam *last);

#endif

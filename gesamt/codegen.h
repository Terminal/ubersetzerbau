#ifndef CODEGEN_H_INCLUDED
#define CODEGEN_H_INCLUDED

#ifndef BFE
typedef struct burm_state *STATEPTR_TYPE;
#endif
/*
#else
#define STATEPTR_TYPE int
#endif
*/

typedef struct parameter {
    int position;
    int stackposition;
    struct parameter *next;
    struct parameter *previous;
} callparam;

typedef struct s_node {
    int	op;
    struct s_node *kids[2];
    STATEPTR_TYPE state;
    
    /* user defined data structures */
    char *regname;
    char *regnamesrc;
    char *regnameindex;
    int value;
    int stackposition;
    char *falselbl;
    char *truelbl;
    char *afterjmplbl;
    callparam *params;
} treenode;

enum operator {
    STATS,
    RETURNOP,
    ADD,
    SUB,
    MULT,
    CONST,
    VARACCESS,
    ARRAYACCESS,
    BOOLOR,
    BOOLSMALLER,
    BOOLNOTEQ,
    IFTHEN,
    LABEL,
    WHILELOOP,
    ASSIGNVAR,
    ASSIGNARRAY,
    ARRAYINDEX,
    DUMMY,
    SAVE,
    PARAM,
    CALL,
    CALLVOID,
    FUNCSTART,
    FUNCEND
};

typedef treenode *treenodep;

#define NODEPTR_TYPE	treenodep
#define OP_LABEL(p)	((p)->op)
#define LEFT_CHILD(p)	((p)->kids[0])
#define RIGHT_CHILD(p)	((p)->kids[1])
#define STATE_LABEL(p)	((p)->state)
#define PANIC		printf

#endif

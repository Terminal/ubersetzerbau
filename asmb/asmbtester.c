#include <stdio.h>
#include <stddef.h>

int compare(char *, int);
char *result_to_s(int);

int main()
{
    char *format = "%-70s,%-5i : %s\n";
    char *test1 = "Hello worldwithsomepaddingwithoutspacesandmuchmorepadding";
    char *test2 = "Hello,worldwithsomepaddingwithoutspacesandmuchmorepadding";
    char *test3 = "Hello world with many spaces and some padding with much more spaces";
    char *test4 = "                     ";
    char emptystring[20];
    int i=0;

    for(i=0; i<20; i++)
    {
        emptystring[i] = 0;
    }


    printf(format,test1,20, result_to_s(compare(test1, 20)));  
    printf(format,test2,22, result_to_s(compare(test2, 22)));

    for(i=0; i<=50; i++)
    {
	printf(format,test3,i, result_to_s(compare(test3, i)));
    }

    for(i=0; i<=5; i++)
    {
	printf(format,"spaces",i, result_to_s(compare(test4, i)));
    }

    for(i=0; i<=4; i++)
    {
	printf(format,"empty string",i, result_to_s(compare(emptystring, i)));
    }

    return 0;
}

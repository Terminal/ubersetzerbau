	.data
.MASK:
	.string	"                " # comparison mask
	
	.text
.globl asmb
	.type	asmb, @function
asmb:
.LFB0:
	.cfi_startproc
	enter	$0, $0

	movq $0, %rax
	
	# prepare the comparison mask
	movdqu	.MASK, %xmm9

.STARTLOOP:
	# loop over the string in blocks of 16 bytes
	# until the counter reaches 0
	cmp $0, %rsi
	jle .ENDLOOP
	
	# perform the comparison
	movdqu	(%rdi), %xmm8
	pcmpeqb	%xmm9, %xmm8
	pmovmskb %xmm8, %r8

#
# Handle the special case of n < 16
#
	cmp $16,%rsi
	jge .NORMALRUN

	# calculate the overlap
	neg %rsi
	leaq 16(%rsi), %rcx

	# fix the comparison result
	shl %cl, %r8

	# clear the outshifted bits
	# (because a maximum of 16 bits is shifted it is sufficient to
	# clear the 2 neighbored bytes)
	and $65535, %r8
	
.NORMALRUN:
	popcnt		%r8, %r8
	addq		%r8, %rax

	# adjust counter and pointer
	subq		$16, %rsi
	addq		$16, %rdi
	
	jmp .STARTLOOP

.ENDLOOP:
	leave
	ret
	.cfi_endproc
	
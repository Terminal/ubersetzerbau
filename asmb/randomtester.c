#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

int compare(char *, int);
int asmb(char *, int);
int spacecount(char *, int);

#define MAX_LINE_LENGTH 50;

int main() {
	int k=0;
    
    while(1)
    {
	srand(time(NULL));
	int length = rand() % MAX_LINE_LENGTH;
	int n = rand() % (length - 16);
	char *line = (char *)malloc(length); 

	for(int i=0; i<length; i++)
	{
	    line[i] = (char) (32 + (rand() % 128));
	}

	int result = compare(line, n);

	if(result)
	{
	    printf("PASS: %i\n", k);
	}
	else 
	{
	    printf("FAIL: %i, %s, asm: %i, c: %i\n", n, line, asmb(line, n), spacecount(line, n));
	    return 1;
	}
      
	k++;

	free(line);
    }
}



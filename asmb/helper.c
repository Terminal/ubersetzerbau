#include<stdlib.h>

size_t asmb(char *, int);

char *result_to_s(int result)
{
    if(result)
    {
	return "PASS";
    }
    else 
    {
	return "FAIL";
    }
}


size_t spacecount(char *s, size_t n)
{
    int c=0;
    int i;
    for(i=0; i<n;i++)
    {
        if(s[i]==' ')
            c++;
    }
    return c;
}

int compare(char * text, size_t n)
{
    return asmb(text, n) == spacecount(text, n);
}

	.file	"asmbtester.c"
	.text
.globl spacecount
	.type	spacecount, @function
spacecount:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	movq	%rsp, %rbp
	.cfi_offset 6, -16
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$0, -8(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L2
.L4:
	movl	-4(%rbp), %eax
	cltq
	addq	-24(%rbp), %rax
	movzbl	(%rax), %eax
	cmpb	$32, %al
	jne	.L3
	addl	$1, -8(%rbp)
.L3:
	addl	$1, -4(%rbp)
.L2:
	movl	-4(%rbp), %eax
	cltq
	cmpq	-32(%rbp), %rax
	jb	.L4
	movl	-8(%rbp), %eax
	cltq
	leave
	ret
	.cfi_endproc
.LFE0:
	.size	spacecount, .-spacecount
.globl compare
	.type	compare, @function
compare:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	movq	%rsp, %rbp
	.cfi_offset 6, -16
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	.cfi_offset 3, -24
	call	asmb
	movslq	%eax,%rbx
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	spacecount
	cmpq	%rax, %rbx
	sete	%al
	movzbl	%al, %eax
	addq	$24, %rsp
	popq	%rbx
	leave
	ret
	.cfi_endproc
.LFE1:
	.size	compare, .-compare
	.section	.rodata
	.align 8
.LC0:
	.string	"Hello worldwithsomepaddingwithoutspacesandmuchmorepadding"
	.align 8
.LC1:
	.string	"Hello,worldwithsomepaddingwithoutspacesandmuchmorepadding"
	.align 8
.LC2:
	.string	"Hello world with many spaces and some padding with much more spaces"
.LC3:
	.string	"%s: %i\n"
	.text
.globl main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	movq	%rsp, %rbp
	.cfi_offset 6, -16
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	$.LC0, -24(%rbp)
	movq	$.LC1, -16(%rbp)
	movq	$.LC2, -8(%rbp)
	movq	-24(%rbp), %rax
	movl	$20, %esi
	movq	%rax, %rdi
	call	compare
	movl	%eax, %edx
	movl	$.LC3, %eax
	movq	-24(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
	movq	-16(%rbp), %rax
	movl	$22, %esi
	movq	%rax, %rdi
	call	compare
	movl	%eax, %edx
	movl	$.LC3, %eax
	movq	-16(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
	movq	-8(%rbp), %rax
	movl	$25, %esi
	movq	%rax, %rdi
	call	compare
	movl	%eax, %edx
	movl	$.LC3, %eax
	movq	-8(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	leave
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Debian 4.4.5-8) 4.4.5"
	.section	.note.GNU-stack,"",@progbits

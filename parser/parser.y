%{
    #include <stdio.h>
    #include <stdlib.h>
%}

%start Program
%token ID NUM END ARRAY OF INT RETURN IF THEN ELSE WHILE DO VAR NOT OR ASSIGN
%%
Program:	/* empty */
		| Funcdef ';' Program
		;

Funcdef:	ID '(' Pars ')' Stats END
		;

Pars:		/* empty */
		| Vardefs
		;

Vardefs:	Vardef Vardefrest
		;

Vardef:		ID ':' Type
		;

Vardefrest:	/* empty */
		| Vardefrest ',' Vardef
		;

Type:		Arrayinit INT
		;

Arrayinit:	/* empty */
		| ARRAY OF Arrayinit
		;

Stats:		/* empty */
		| Stat ';' Stats

Stat:		RETURN Expr
		| IF Bool THEN Stats END
		| IF Bool THEN Stats ELSE Stats END
		| WHILE Bool DO Stats END
		| VAR Vardef ASSIGN Expr
		| Lexpr ASSIGN Expr
		| Term
		;

Bool:		Bterm Boolrest
		;

Boolrest:	/* empty */
		| OR Bool
		;

Bterm: 		'(' Bool ')'
       		| NOT Bterm
		| Expr '<' Expr
		| Expr '#' Expr
		;

Lexpr:		ID
		| Term '[' Expr ']'
		; 

Expr:		Term
		| Term '-' Term Subterm
		| Term '+' Term Addterm
		| Term '*' Term Multterm
		;

Subterm:	/* empty */
		| '-' Term Subterm
		;

Addterm:	/* empty */
		| '+' Term Addterm
		;

Multterm:	/* empty */
		| '*' Term Multterm
		;

Term:		'(' Expr ')'
		| NUM
		| Term '[' Expr ']'
		| ID
		| ID '(' Callparam ')' ':' Type
		;

Callparam:	/* empty */
		| Expr
		| Expr Callparamrest
		;

Callparamrest:	',' Expr Callparamrest
		| ',' Expr
		;

%%

int yywrap(void) {
    return 1;
}

int yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
    exit(2);
}

int main(void) {
    yyparse();
    return 0;
}

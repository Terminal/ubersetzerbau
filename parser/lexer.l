%{
	#include "y.tab.h"
	void yyerror(char *);
%}

keyword		(end|array|of|int|return|if|then|else|while|do|var|not|or)
specialchar	(;|\(|\)|\,|\:|\<|\#|\[|\]|\-|\+|\*)
assign		\:\=
identifier	[a-zA-Z]+[a-zA-Z0-9]*
decnumber	[0-9]+
hexnumber	\$[0-9a-fA-F]+
%%
[ \t\n]+		
\-\-.*						
end		printf("%s\n", yytext); return END;
array	        printf("%s\n", yytext); return ARRAY;
of		printf("%s\n", yytext); return OF;
int		printf("%s\n", yytext); return INT;
return		printf("%s\n", yytext); return RETURN;
if		printf("%s\n", yytext); return IF;
then		printf("%s\n", yytext); return THEN;
else		printf("%s\n", yytext); return ELSE;
while		printf("%s\n", yytext); return WHILE;
do		printf("%s\n", yytext); return DO;
var		printf("%s\n", yytext); return VAR;
not		printf("%s\n", yytext); return NOT;
or		printf("%s\n", yytext); return OR;
{assign}	printf("%s\n", yytext); return ASSIGN;
{specialchar}	printf("%s\n", yytext); return yytext[0];
{identifier}	printf("id\n"); return ID;
{decnumber}	printf("num\n"); return NUM;
{hexnumber}	printf("num\n"); return NUM;
.		{ printf("bad input character '%s' at line %d\n", yytext, yylineno); exit(1); }


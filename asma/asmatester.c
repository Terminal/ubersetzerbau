#include <stdio.h>
#include <string.h>

int asma(char *);

int spacecount(char *s)
{
    int c=0;
    int i;
    for(i=0; i<16;i++)
    {
	if(s[i]==' ')
	    c++;
    }
    return c;


}

char *compare(char * text)
{
    int result =  asma(text) == spacecount(text);

    if(result)
    {
	return "PASS";
    }
    else
    {
	return "FAIL";
    }
}

int main()
{
    char *test1 = "Hello worldwithsomepaddingwithoutspaces";
    char *test2 = "Hello,worldwithsomepaddingwithoutspaces";
    char *test3 = "Hello world with many spaces";
    char *test4 = "Hello world wit";
    char *test5 = "               ";
    char *test6 = "                                       ";
    char emptystring[20];
    int i=0;

    for(i=0; i<20; i++)
    {
	emptystring[i] = 0;
    }

    printf("%-60s %s\n", test1, compare(test1));  
    printf("%-60s %s\n", test2, compare(test2));
    printf("%-60s %s\n", test3, compare(test3));
    printf("%-60s %s\n", test4, compare(test4));
    printf("%i spaces %+55s\n", strlen(test5), compare(test5));
    printf("%i spaces %+55s\n", strlen(test6), compare(test6));
    printf("empty string %+52s\n", compare(emptystring));
    return 0;
}

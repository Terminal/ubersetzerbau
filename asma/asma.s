	.data
.MASK:
	.string	"                " # comparison mask
	
	.text
.globl asma
	.type	asma, @function
asma:
.LFB0:
	.cfi_startproc
	enter	$0, $0
# prepare comparison registers
	movdqu	(%rdi), %xmm8
	movdqu	.MASK, %xmm9
	
# do the comparison
	pcmpeqb		%xmm8, %xmm9
# extract the result
	pmovmskb	%xmm9, %rax
	popcnt		%rax, %rax
	leave
	ret
	.cfi_endproc
	
keyword		(end|array|of|int|return|if|then|else|while|do|var|not|or)
specialchar	(;|\(|\)|\,|\:|\:=|\<|\#|\[|\]|\-|\+|\*)
identifier	[a-zA-Z]+[a-zA-Z0-9]*
decnumber	[0-9]+
hexnumber	\$[0-9a-fA-F]+
%%
[ \t\n]+		
\-\-.*						
{keyword}	printf("%s\n", yytext);
{specialchar}	printf("%s\n", yytext);
{identifier}	printf("id %s\n", yytext);
{decnumber}	printf("num %x\n", strtol(yytext, NULL, 10));
{hexnumber}	printf("num %x\n", strtol(yytext+1, NULL, 16));
.		{ printf("bad input character '%s' at line %d\n", yytext, yylineno); exit(1); }

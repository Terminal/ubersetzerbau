#include "ast.h"

#include <stdlib.h>
#include <stdio.h>


treenodep create_operator_node (enum operator op, treenodep left, treenodep right)
{
  treenodep node = malloc (sizeof (treenode));

  if (node == NULL)
    {
      printf ("Unable to allocate a treenode\n");
      exit (100);
    }

  node->op = op;
  node->kids[0] = left;
  node->kids[1] = right;
  node->regname = NULL;
  node->value = 0;
}

treenodep combine_arithmetic_trees (treenodep left, treenodep right)
{
  if (right == NULL)
    {
      return left;
    }

  right->kids[0] = left;
  return right;
}

treenodep create_const_node (int const_value)
{
  treenodep node = create_operator_node (CONST, NULL, NULL);
  node->value = const_value;
  return node;
}

treenodep create_var_node (void)
{
  treenodep node = create_operator_node (VARACCESS, NULL, NULL);
  return node;
}

treenodep create_bool_node (enum operator op, treenodep left, treenodep right, char *truelbl, char *falselbl)
{
    treenodep node = create_operator_node (op, left, right);
    node->truelbl = truelbl;
    node->falselbl = falselbl;
    return node;
}

treenodep create_if_node (treenodep bool, treenodep stats, char *falselbl, char *afterjmp)
{
    treenodep statsnode = stats;
    if (stats == NULL)
	statsnode = create_operator_node (DUMMY, NULL, NULL);

    treenodep node = create_operator_node (IFTHEN, bool, stats);
    node->falselbl = falselbl;
    node->afterjmplbl = afterjmp;
    return node;
}

treenodep create_if_else_node (treenodep bool, treenodep statsthen, treenodep statselse, char *falselbl)
{
    if (statsthen == NULL && statselse == NULL)
	return create_operator_node (DUMMY, NULL, NULL);
    
    if (statselse == NULL)
	return create_if_node (bool, statsthen, falselbl, NULL);

    if (statsthen == NULL)
	statsthen = create_operator_node (DUMMY, NULL, NULL);

    char *afterjmplbl = new_label (); 

    treenodep ifnode = create_if_node (bool, statsthen, falselbl,  afterjmplbl);
    treenodep elsenode = create_stats_node (statselse, create_label_node (afterjmplbl));

    return create_stats_node (ifnode, elsenode);
}

treenodep create_stats_node (treenodep left, treenodep right)
{
    if (left == NULL)
	left = create_operator_node (DUMMY, NULL, NULL);

    if (right != NULL)
    {
	treenodep node = create_operator_node (STATS, left, right);
	return node;
    }

    return left;
}

treenodep create_label_node (char *lbl)
{
    treenodep node = create_operator_node (LABEL, NULL, NULL);
    node->afterjmplbl = lbl;
    return node;
}

treenodep create_while_node (treenodep bool, treenodep stats, char *falselbl)
{
    if (stats == NULL)
	stats = create_operator_node (DUMMY, NULL, NULL);

    char *beginlbl = new_label();
    treenodep beginnode = create_label_node (beginlbl);
    treenodep whilenode = create_operator_node (WHILELOOP, bool, stats);
    whilenode->afterjmplbl = beginlbl;
    whilenode->falselbl = falselbl;
    return create_stats_node (beginnode, whilenode);
}

treenodep create_var_assign_node (treenodep left)
{
    treenodep node = create_operator_node (ASSIGNVAR, left, NULL);
    return node;
}

treenodep create_array_assign_node (treenodep array, treenodep value)
{
    treenodep node = create_operator_node (ASSIGNARRAY, array, value);
    return node;
}

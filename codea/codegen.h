#ifndef CODEGEN_H_INCLUDED
#define CODEGEN_H_INCLUDED


#ifndef BFE
typedef struct burm_state *STATEPTR_TYPE;
#endif
/*
#else
#define STATEPTR_TYPE int
#endif
*/

typedef struct s_node {
    int	op;
    struct s_node *kids[2];
    STATEPTR_TYPE state;
    
    /* user defined data structures */
    char *regname;
    char *regnamesrc;
    int value;
} treenode;

enum operator {
    RETURNOP,
    ADD,
    SUB,
    MULT,
    CONST,
    VARACCESS,
    ARRAYACCESS,
    FUNCSTART,
    FUNCEND
};

typedef treenode *treenodep;

#define NODEPTR_TYPE	treenodep
#define OP_LABEL(p)	((p)->op)
#define LEFT_CHILD(p)	((p)->kids[0])
#define RIGHT_CHILD(p)	((p)->kids[1])
#define STATE_LABEL(p)	((p)->state)
#define PANIC		printf

#endif

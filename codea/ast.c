#include "ast.h"

#include <stdlib.h>
#include <stdio.h>


treenodep create_operator_node (enum operator op, treenodep left, treenodep right)
{
  treenodep node = malloc (sizeof (treenode));

  if (node == NULL)
    {
      printf ("Unable to allocate a treenode\n");
      exit (100);
    }

  node->op = op;
  node->kids[0] = left;
  node->kids[1] = right;
  node->regname = NULL;
  node->value = 0;
}

treenodep combine_arithmetic_trees (treenodep left, treenodep right)
{
  if (right == NULL)
    {
      return left;
    }

  right->kids[0] = left;
  return right;
}

treenodep create_const_node (int const_value)
{
  treenodep node = create_operator_node (CONST, NULL, NULL);
  node->value = const_value;
  return node;
}

treenodep create_var_node (void)
{
  treenodep node = create_operator_node (VARACCESS, NULL, NULL);
  return node;
}


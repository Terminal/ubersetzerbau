#ifndef CODEGEN_ASM_H_DEFINED
#define CODEGEN_ASM_H_DEFINED

#include <stdio.h>
#include <stdlib.h>
#include "symtable.h"
#include "codegen.h"
#include <assert.h>
#include <string.h>

void assign_parameter_regs (struct symtable_entry *last);
void assign_var_reg (treenodep t);

char *get_next_reg (void);
char *get_next_parameter_reg (void);

void function_start (char *name);
void function_end ();

void return_reg (char *regname);
void return_const (int value);

void move (char *regname1, char *regname2);
void move_const (int value, char *regname);

void sub (char *regname1, char *regname2);
void sub_const_left (int value, char *regname);
void sub_const_right (char *regname, int value);

void add (char *regname1, char *regname2);
void add_const (int value, char *regname);

void mul (char *regname1, char *regname2);
void mul_const (int value, char *regname);

void array (char *regname1, char *reganme2, char *dstreg);
void array_const (char *regname, int index, char *dstreg);

#endif

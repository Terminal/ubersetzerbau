#ifndef SEMANTIC_CHECK_H_INCLUDED
#define SEMANTIC_CHECK_H_INCLUDED

enum type { INTEGER }; 
 
struct symbol_type {
    enum type symb_type;   
    
    /* the nesting depth of arrays, 0 if no array */
    int depth;    
};
    
struct symtable_entry {
    /* the name of the identifier */
    char *name;
    
    struct symbol_type *type;

    char *regname;
    
    /* list meta data */
    struct symtable_entry *next;
    struct symtable_entry *prev;
};

struct symtable_entry *append_to_symtable (struct symtable_entry *last, struct symtable_entry *lastother);
struct symtable_entry *create_symtable_entry (char *name, struct symbol_type *type); 
struct symtable_entry *lookup_from_symboltable(char *name, struct symtable_entry *last);
struct symbol_type *create_int_symbol_type (int depth);

void check_assignment (struct symbol_type *first_type, struct symbol_type *second_type);
void check_arithmetic_expression (struct symbol_type *first_type, struct symbol_type *second_type);
void check_symt_collision (struct symtable_entry *last);
void check_is_array_type (struct symbol_type *type);
void check_is_int_type (struct symbol_type *type);
struct symbol_type *get_type_of_id (char *name, struct symtable_entry *last);
char *get_reg_of_id (char *name, struct symtable_entry *last);
void print_symbol_table (struct symtable_entry *last);

#endif

#include "codegen_asm.h"

static int used_param_registers[6];
static char *param_registers[] = { "rdi", "rsi", "rdx", "rcx", "r8", "r9" };
static int used_registers[3];
static char *registers[] = { "rax", "r10", "r11" };

void assign_parameter_regs (struct symtable_entry *last)
{
    struct symtable_entry *cur = last;
   
    if (last == NULL)
	return;

    while (cur->prev != NULL)
    {
	cur = cur->prev;
    }

    /* Iterate over all parameters */
    while (cur != last->next)
    {
	cur->regname = get_next_parameter_reg ();
	cur = cur->next;
    }

}

void assign_var_reg (treenodep t)
{
    t->regname = get_next_reg ();
}

static char *find_reg (int *usage_bitmap, char **register_map, int num_registers)
{
    int i=0;

    for (i=0; i < num_registers; i++)
    {
	if (usage_bitmap[i] == 0)
	{
	    usage_bitmap[i] = 1;
	    return register_map[i];
	}
    }

    return NULL;
}


char *get_next_parameter_reg (void)
{
    int num_registers = sizeof (used_param_registers) / sizeof (int);
    char *result_reg = NULL;

    result_reg = find_reg (used_param_registers, param_registers, num_registers);

    if (result_reg != NULL)
    {
	return result_reg;
    }

    printf ("No parameter registers left\n");

    exit (100);
}

char *get_next_reg (void)
{
    int num_registers = sizeof (used_registers) / sizeof (int);
    char *result_reg = NULL;

    result_reg = find_reg (used_registers, registers, num_registers);

    if (result_reg != NULL)
    {
	return result_reg;
    }

    result_reg = get_next_parameter_reg ();

    if (result_reg != NULL)
    {
	return result_reg;
    }

    printf ("No registers left \n");
    exit (100);
}

void free_reg (treenodep tree)
{
    int num_param_registers = sizeof (used_param_registers) / sizeof (int);
    int num_registers = sizeof (used_registers) / sizeof (int);
    char *name;
    int i;

    assert (tree->kids[0] != NULL);
    assert (tree->kids[1] != NULL);

    if (tree->kids[0]->op == CONST)
    {
	name = tree->kids[0]->regname;
    }
    else
    {
	name = tree->kids[1]->regname;
    }

    for (i=0; i < num_param_registers; i++)
    {
	if (strcmp (name, param_registers[i]) == 0)
	{
	    used_param_registers[i] = 0;
	    return;
	}
    }

    for (i=0; i < num_registers; i++)
    {
	if (strcmp (name, registers[i]) == 0)
	{
	    used_registers[i] = 0;
	    return;
	}
    }
}

void function_start (char *name)
{
    printf (".globl %s\n", name);
    printf ("\t.type\t%s, @function\n", name);
    printf ("%s:\n", name);
    printf ("\tenter $0, $0\n");
}

void function_end ()
{
    memset(used_param_registers, 0, sizeof (used_param_registers));
    memset(used_registers, 0, sizeof (used_registers));
}

void return_reg (char *regname)
{
    move (regname, "rax");
    printf ("\tleave\n");
    printf ("\tret\n");
}

void return_const (int value)
{
    move_const (value, "rax");
    printf ("\tleave\n");
    printf ("\tret\n");
}

void move_const (int value, char *regname)
{
    printf ("\tmovq $%i, %%%s\n", value, regname);
}

void move (char *regname1, char *regname2)
{

    if (strcmp (regname1, regname2) == 0)
    {
	return;
    }

    printf ("\tmovq %%%s, %%%s\n", regname1, regname2);
}

void add (char *regname1, char *regname2)
{
    printf ("\taddq %%%s, %%%s\n", regname1, regname2);
}

void add_const (int value, char *regname)
{
    printf ("\taddq $%i, %%%s\n", value, regname);
}

void mul (char *regname1, char *regname2)
{
    printf ("\timulq %%%s, %%%s\n", regname1, regname2);
}

void mul_const (int value, char *regname)
{
    printf ("\timulq $%i, %%%s\n", value, regname);
}

void sub (char *regname1, char *regname2)
{
    printf ("\tsubq %%%s, %%%s\n", regname1, regname2);
}

void sub_const_left (int value, char *regname)
{
    /* Because there is no command for constant - register
       use the following equivalence:
       10 - x = r | * (-1)
       -10 + x = -r
       r = (-1) * (-r)
    */

    value = value * -1;
    add_const (value, regname);
    printf ("\tneg %%%s\n", regname);
}

void sub_const_right (char *regname, int value)
{
    printf ("\tsubq $%i, %%%s\n", value, regname);
}

void array_const (char *regname, int index, char *dstreg)
{
    printf ("\tmovq %i(%%%s), %%%s\n", (index * 8), regname, dstreg); 
}

void array (char *basereg, char *indexreg, char *dstreg)
{
    printf ("\tmovq (%%%s,%%%s, 8), %%%s\n", basereg, indexreg, dstreg);
}

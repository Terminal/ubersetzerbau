#ifndef CODEGEN_TREE_H_INCLUDED
#define CODEGEN_TREE_H_INCLUDED

#include "codegen.h"
#include "codegen_asm.h"
#include "symtable.h"

treenodep create_operator_node (enum operator op, treenodep left, treenodep right);
treenodep combine_arithmetic_trees (treenodep left, treenodep right);
treenodep create_const_node (int const_value);
treenodep create_reg_node (char *name);
treenodep create_var_node (void);

void assign_parameter_regs (struct symtable_entry *last);
void assign_var_register (treenodep t);

char *get_next_reg (void);
char *get_next_parameter_reg (void);


#endif

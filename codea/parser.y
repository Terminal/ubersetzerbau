%{
    #include <stdlib.h>
    #include <string.h>
    #include "ast.h"
    #include "symtable.h"
    #include "codegen_asm.h"
    
%}

%start Program
%token ID NUM END ARRAY OF INT RETURN IF THEN ELSE WHILE DO VAR NOT OR ASSIGN

@autoinh symt

/* inherited attributes */
@attributes { struct symtable_entry *symt; } Bool Bterm Boolrest Callparam Callparamrest

/* synthesized attributes */
@attributes { struct symtable_entry *symtup; } Pars Vardefs Vardef Vardefrest
@attributes { char *x; } ID
@attributes { int val; } NUM
@attributes { int depth; } Type Arrayinit

/* both */
@attributes { treenodep tree; struct symtable_entry *symt; } Stats
@attributes { treenodep tree; } Program Funcdef
@attributes { struct symtable_entry *symt; struct symtable_entry *symtup; treenodep tree; } Stat
@attributes { struct symtable_entry *symt; struct symbol_type *typeup; } Lexpr
@attributes { struct symtable_entry *symt; struct symbol_type *typeup; treenodep tree; } Expr Term Subterm Addterm Multterm

@traversal @postorder typecheck
@traversal @postorder codegen
%%

Program:	/* empty */
		@{ @i @Program.0.tree@ = NULL; @}
		| Funcdef ';' Program
		@{ 
		   @i @Program.0.tree@ = NULL;
		@}
		;

Funcdef:	ID '(' Pars ')' Stats END
		@{ 
    		   @i @Stats.0.symt@ = @Pars.0.symtup@;
		   @i @Funcdef.0.tree@ = NULL;
		   @codegen
		       function_start (@ID.0.x@);

		       if (@Stats.0.tree@ != NULL)
		       	  invoke_burm (@Stats.0.tree@);

		       function_end ();
		@}			
		;

Pars:		/* empty */
		@{ @i @Pars.0.symtup@ = NULL; @}

		| Vardefs
		@{ 
		   @i @Pars.0.symtup@ = @Vardefs.0.symtup@; 
		   @typecheck check_symt_collision (@Vardefs.0.symtup@);
		 
		   /* assign a register to every input parameter */
		   @codegen assign_parameter_regs (@Pars.0.symtup@);
		@}
		;

Vardefs:	Vardef Vardefrest
		@{ @i @Vardefs.0.symtup@ = append_to_symtable (@Vardef.0.symtup@, @Vardefrest.0.symtup@); @}
		;

Vardef:		ID ':' Type
		@{ @i @Vardef.0.symtup@ = create_symtable_entry (@ID.0.x@, create_int_symbol_type(@Type.0.depth@)); @}
		;

Vardefrest:	/* empty */
		@{ @i @Vardefrest.0.symtup@ = NULL; @}
		
		| Vardefrest ',' Vardef
		@{ @i @Vardefrest.0.symtup@ = append_to_symtable (@Vardefrest.1.symtup@, @Vardef.0.symtup@); @}
		;

Type:		Arrayinit INT
		@{ @i @Type.0.depth@ = @Arrayinit.0.depth@; @}
		;

Arrayinit:	/* empty */
		@{ @i @Arrayinit.0.depth@ = 0; @}
		| ARRAY OF Arrayinit
		@{ @i @Arrayinit.0.depth@ = 1 + @Arrayinit.1.depth@; @}
		;

Stats:		/* empty */
		@{ @i @Stats.0.tree@ = NULL; @}
		| Stat ';' Stats
		@{ 
		    /* The symbol table may have changed in the first part because of a variable definition */
		    @i @Stats.1.symt@ = @Stat.0.symtup@;

		    /* For now take only the first tree */ 
		    @i @Stats.0.tree@ = @Stat.0.tree@;
		@}

Stat:		RETURN Expr
		@{ 
		   @i @Stat.0.symtup@ = @Stat.0.symt@; 
		   @i @Stat.0.tree@ = create_operator_node (RETURNOP, @Expr.0.tree@, NULL);
		@}

		/* TODO: For now most other statements have no operator tree */

		| IF Bool THEN Stats END
		@{ 
		   @i @Stat.0.symtup@ = @Stat.0.symt@; 
		   @i @Stat.0.tree@ = NULL;
		@}
		
		| IF Bool THEN Stats ELSE Stats END
		@{ 
		   @i @Stat.0.symtup@ = @Stat.0.symt@; 
		   @i @Stat.0.tree@ = NULL;
		@}

		| WHILE Bool DO Stats END
		@{ 
		   @i @Stat.0.symtup@ = @Stat.0.symt@; 
		   @i @Stat.0.tree@ = NULL;
		@}
		
		| VAR Vardef ASSIGN Expr
		@{ 
		    @i @Stat.0.symtup@ = append_to_symtable (@Stat.0.symt@, @Vardef.0.symtup@);
		    @i @Stat.0.tree@ = NULL;
		    @typecheck	check_assignment (@Vardef.0.symtup@->type, @Expr.0.typeup@);			
		@}

		| Lexpr ASSIGN Expr
		@{ 
		    @i @Stat.0.symtup@ = @Stat.0.symt@;
		    @i @Stat.0.tree@ = NULL;
		    @typecheck check_assignment (@Lexpr.0.typeup@, @Expr.0.typeup@);
		@}   
		   
		| Term
		@{ 
		   @i @Stat.0.symtup@ = @Stat.0.symt@;
		   @i @Stat.0.tree@ = @Term.0.tree@;
		@}
		;

Bool:		Bterm Boolrest
		;

Boolrest:	/* empty */
		| OR Bool
		;

Bterm: 		'(' Bool ')'
       		| NOT Bterm
		| Expr '<' Expr
		@{ @typecheck check_arithmetic_expression (@Expr.0.typeup@, @Expr.1.typeup@); @}

		| Expr '#' Expr
		@{ @typecheck check_arithmetic_expression (@Expr.0.typeup@, @Expr.1.typeup@); @}
		;

Lexpr:		ID
		@{ @i @Lexpr.0.typeup@ = get_type_of_id (@ID.0.x@, @Lexpr.0.symt@); @}
		| Term '[' Expr ']'
		@{ 
		   @i @Lexpr.0.typeup@ = create_int_symbol_type (@Term.0.typeup@->depth - 1); 
		   @typecheck check_is_array_type (@Term.0.typeup@); check_is_int_type (@Expr.0.typeup@);
		@}
		; 

Expr:		Term
		@{ 
		   @i @Expr.0.typeup@ = @Term.0.typeup@; 
		   @i @Expr.0.tree@ = @Term.0.tree@;
		@}
		
		/* because the types of all NTs have to be the same, we can just the first one */
		| Subterm '-' Term
		@{ 
		    @i @Expr.0.typeup@ = @Term.0.typeup@;
		    @i @Expr.0.tree@ = create_operator_node (SUB, @Subterm.0.tree@, @Term.0.tree@); 
 		    @typecheck 
		    	check_arithmetic_expression (@Term.0.typeup@, @Subterm.0.typeup@);
		    @codegen
			@Expr.0.tree@->regname = @Subterm.0.tree@->regname;			
		@}

		| Addterm '+' Term
		@{ 
		    @i @Expr.0.typeup@ = @Term.0.typeup@; 
		    @i @Expr.0.tree@ = create_operator_node (ADD, @Addterm.0.tree@, @Term.0.tree@);
		    @typecheck 
		    	check_arithmetic_expression (@Term.0.typeup@, @Addterm.0.typeup@);
		    @codegen
			@Expr.0.tree@->regname = @Addterm.0.tree@->regname;			
		@}

		| Multterm '*' Term
		@{ 
		    @i @Expr.0.typeup@ = @Term.0.typeup@; 
		    @i @Expr.0.tree@ = create_operator_node (MULT, @Multterm.0.tree@, @Term.0.tree@);
		    @typecheck 
		    	check_arithmetic_expression (@Term.0.typeup@, @Multterm.0.typeup@);
		    @codegen
			@Expr.0.tree@->regname = @Multterm.0.tree@->regname;
		@}
		;

Subterm:	Term
		@{ 
		   @i @Subterm.0.typeup@ = @Term.0.typeup@; 
		   @i @Subterm.0.tree@ = @Term.0.tree@;
		@}

		| Subterm '-' Term
		@{ 
		    @i @Subterm.0.typeup@ = @Term.0.typeup@; 
		    @i @Subterm.0.tree@ = create_operator_node (SUB, @Subterm.1.tree@, @Term.0.tree@);
		    @typecheck 
			check_arithmetic_expression (@Term.0.typeup@, @Subterm.0.typeup@);
		    @codegen
			@Subterm.0.tree@->regname = @Subterm.1.tree@->regname;
			free_reg (@Subterm.0.tree@);
		@}
		;

Addterm:	Term
		@{ 
		   @i @Addterm.0.typeup@ = @Term.0.typeup@; 
		   @i @Addterm.0.tree@ = @Term.0.tree@;
		@}

		| Addterm '+' Term
		@{ 
		    @i @Addterm.0.typeup@ = @Term.0.typeup@; 
		    @i @Addterm.0.tree@ = create_operator_node (ADD, @Addterm.1.tree@, @Term.0.tree@);
		    @typecheck 
			check_arithmetic_expression (@Term.0.typeup@, @Addterm.0.typeup@);
		    @codegen
			@Addterm.0.tree@->regname = @Addterm.1.tree@->regname;
			free_reg (@Addterm.0.tree@);
		@}
		;

Multterm:	Term
		@{ 
		   @i @Multterm.0.typeup@ = @Term.0.typeup@; 
		   @i @Multterm.0.tree@ = @Term.0.tree@;
		@}

		| Multterm '*' Term
		@{ 
		    @i @Multterm.0.typeup@ = @Term.0.typeup@;
		    @i @Multterm.0.tree@ = create_operator_node (MULT, @Multterm.1.tree@, @Term.0.tree@);		  
		    @typecheck 
			check_arithmetic_expression (@Term.0.typeup@, @Multterm.0.typeup@);
		    @codegen
			@Multterm.0.tree@->regname = @Multterm.1.tree@->regname;
			free_reg (@Multterm.0.tree@);
		@}
		;

Term:		'(' Expr ')'
		@{ 
		   @i @Term.0.typeup@ = @Expr.0.typeup@; 
		   @i @Term.0.tree@ = @Expr.0.tree@;
		@}

		| NUM
		@{ 
		   @i @Term.0.typeup@ = create_int_symbol_type (0); 
		   @i @Term.0.tree@ = create_const_node (@NUM.0.val@);
		   @codegen @Term.0.tree@->regname = get_next_reg ();
		@}

		| Term '[' Expr ']'
		/* this term reduces the nesting depth of the array type */
		@{ 
		    @i @Term.0.typeup@ = create_int_symbol_type (@Term.1.typeup@->depth - 1); 
		    @i @Term.0.tree@ = create_operator_node (ARRAYACCESS, @Term.1.tree@, @Expr.0.tree@);		  
		    @typecheck check_is_array_type (@Term.1.typeup@); check_is_int_type (@Expr.0.typeup@);

		    /* reuse the array pointer register */
		    @codegen @Term.0.tree@->regname = @Term.1.tree@->regname;
		@}

		| ID
		@{  
		    @i @Term.0.typeup@ = get_type_of_id (@ID.0.x@, @Term.0.symt@);
		    @i @Term.0.tree@ = create_var_node ();
		    @codegen 
			@Term.0.tree@->regnamesrc = get_reg_of_id (@ID.0.x@, @Term.0.symt@);
			@Term.0.tree@->regname =  get_next_reg ();
		@}

		| ID '(' Callparam ')' ':' Type
		/* because functions are not saved in the symbol table we can create a new type here */
		@{ 
		   @i @Term.0.typeup@ = create_int_symbol_type (@Type.0.depth@); 
		   
		   /* TODO: no function calls are supported yet */
		   @i @Term.0.tree@ = NULL;
		@}
		;

Callparam:	/* empty */
		| Expr
		| Expr Callparamrest
		;

Callparamrest:	',' Expr Callparamrest
		| ',' Expr
		;

%%


int yywrap(void) {
    return 1;
}

int yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
    exit(2);
}

int main(void) {
    yyparse();
    return 0;
}

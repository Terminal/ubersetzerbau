#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "symtable.h"

void print_symbol_table (struct symtable_entry *last)
{
    struct symtable_entry *current = NULL;

    printf ("Symbol table in reverse order: \n");
    
    current = last;
    while (current != NULL)
    {
	printf ("%s, (%i, %i, %s) <- ", current->name, current->type->symb_type, current->type->depth, current->regname);
	current = current->prev;
    }
    printf ("\n");
}

static struct symtable_entry *get_symtable_entry_of_id (char *name, struct symtable_entry *last)
{
    struct symtable_entry *current = NULL;
    current = last;

    while (current != NULL)
    {
	if (strcmp (current->name, name) == 0)
	{
	    return current;
	}

	current = current->prev;
    }

    return NULL;
}

char *get_reg_of_id (char *name, struct symtable_entry *last)
{
    struct symtable_entry *entry = NULL;

    entry = get_symtable_entry_of_id (name, last);

    if (entry != NULL)
    {
	return entry->regname;
    }

    return NULL;
}

struct symbol_type *get_type_of_id (char *name, struct symtable_entry *last)
{
    struct symtable_entry *entry = NULL;

    entry = get_symtable_entry_of_id (name, last);

    if (entry != NULL)
    {
	return entry->type;
    }

    printf ("Undeclared identifier found: %s\n", name);
    print_symbol_table (last);
    exit (3);
}

void check_assignment (struct symbol_type *first_type, struct symbol_type *second_type)
{
    if (first_type == NULL || second_type == NULL)
    {
	printf ("check_assignment: One of the supplied types is NULL\n");
	exit (100);
    }

    if (!is_symbol_type_equal (first_type, second_type))
    {
	printf ("Invalid assignment detected: ");
	printf ("Type depth %i assigned to type depth %i\n", first_type->depth, second_type->depth);
	exit (3);
    }
}

void check_arithmetic_expression (struct symbol_type *first_type, struct symbol_type *second_type)
{
    if (first_type == NULL)
    {
	printf ("check_arithmetic_expression: The required type is NULL\n");
	exit (100);
    }

    if (first_type->depth != 0 || first_type->symb_type != INTEGER)
    {
	printf ("Only basic integer types are allowed in an arithmetic expression\n");
	exit (3);
    }

    if (second_type != NULL)
    {
	if (!is_symbol_type_equal (first_type, second_type))
	{
	    printf ("Invalid expression detected: ");
	    printf ("Both types of an expression must be equal\n");
	    exit (3);
	}
    }
}

void check_symt_collision (struct symtable_entry *last)
{
    struct symtable_entry *current = NULL;
    struct symtable_entry *currentother = NULL;

    if (last == NULL)
    {
	/* no collision possible */
	return;
    }

    current = last;
    currentother = last;
    while (current != NULL)
    {
	while (currentother != NULL)
	{

	    
	    if (current != currentother && strcmp (current->name, currentother->name) == 0)
	    {
		printf ("Duplicate identifier %s detected\n", current->name);
		print_symbol_table (last);
		exit(3);
	    }
	    currentother = currentother->prev;
	}

	currentother = last;
	current = current->prev;
    }
}

void check_is_array_type (struct symbol_type *type)
{
    if (type == NULL)
    {
	printf ("check_is_array_type: supplied type is NULL");
	exit (100);
    }

    if (type->depth == 0 )
    {
	printf ("Invalid type detected. Should be an array type but depth is 0\n");
	exit (3);
    }
}

void check_is_int_type (struct symbol_type *type)
{
    if (type == NULL)
    {
	printf ("check_is_int_type: supplied type is NULL");
	exit (100);
    }

    if (type->symb_type != INTEGER || type->depth != 0)
    {
	printf ("Invalid type detected. Should be %i (0) but is %i (%i)\n", INTEGER, type->symb_type, type->depth);
	exit (3);
    }
}

struct symtable_entry *append_to_symtable (struct symtable_entry *last, struct symtable_entry *lastother) {
    struct symtable_entry *current = NULL;

    if (lastother == NULL)
       return last;

    if (last == NULL)
	return lastother;	

    current = lastother;

    while (current->prev != NULL)
	current = current->prev;

    last->next = current;
    current->prev = last;

    return lastother;
}

struct symtable_entry *lookup_from_symboltable (char *name, struct symtable_entry *last)
{
    struct symtable_entry *current = NULL;
    current = last;

    while (current != NULL)
    {
        if (strcmp (last->name, name) == 0)
        {
            return last;
        }

	current = current->prev;
    }

    return NULL;
}

struct symtable_entry* create_symtable_entry (char *name, struct symbol_type *type)
{
    struct symtable_entry *newentry = NULL;
    newentry = malloc (sizeof (struct symtable_entry));

    if (newentry != NULL)
    {
	newentry->name = name;
	newentry->type = type;
	return newentry;
    }

    printf("Unable to allocate a new symtable entry\n");
    exit(100);

    return NULL;
}

struct symbol_type *create_int_symbol_type (int depth)
{
    struct symbol_type *newsymbtype = NULL;
    newsymbtype = malloc (sizeof (struct symbol_type));

    if(newsymbtype != NULL)
    {
	newsymbtype->symb_type = INTEGER;
	newsymbtype->depth = depth;

	return newsymbtype;
    }

    printf("Unable to allocate a new symbol type\n");
    exit(100);

    return NULL;
}

int is_symbol_type_equal (struct symbol_type *first, struct symbol_type *second)
{
    if(first == NULL || second == NULL)
    {
	return 0;
    }

    return first->symb_type == second->symb_type 
	&& first->depth == second->depth;
}
